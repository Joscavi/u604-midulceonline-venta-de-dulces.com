import React, { Fragment } from 'react'
import Login from './paginas/auth/Login'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import CrearCuenta from './paginas/auth/CrearCuenta';
import Home from './paginas/Home';
import ProductosAdmin from './paginas/productos/ProductosAdmin';
import ProductosCrear from './paginas/productos/ProductosCrear';
import ProductosEditar from './paginas/productos/ProductosEditar';
import ProveedoresAdmin from './paginas/proveedores/ProveedoresAdmin';
import ProveedoresCrear from './paginas/proveedores/ProveedoresCrear';
import ProveedoresEditar from './paginas/proveedores/ProveedoresEditar';
import CategoriasAdmin from './paginas/categorias/CategoriasAdmin';
import CategoriasCrear from './paginas/categorias/CategoriasCrear';
import CategoriasEditar from './paginas/categorias/CategoriasEditar';
import UsuariosAdmin from './paginas/usuarios/UsuariosAdmin';
import UsuariosCrear from './paginas/usuarios/UsuariosCrear';
import UsuariosEditar from './paginas/usuarios/UsuariosEditar';

function App() {
  return (
    <div >
      <Fragment>
        <Router>
          <Routes>
            <Route path="/" exact element = {<Login/>}/>
            <Route path="/crear-cuenta" exact element = {<CrearCuenta/>}/>
            <Route path="/home" exact element = {<Home/>}/>
            <Route path="/productos-admin" exact element={<ProductosAdmin/>}/>
            <Route path="/productos-crear" exact element={<ProductosCrear/>}/>
            <Route path="/productos-editar/:idproducto" exact element={<ProductosEditar/>}/>
            <Route path="/proveedores-admin" exact element={<ProveedoresAdmin/>}/>
            <Route path="/proveedores-crear" exact element={<ProveedoresCrear/>}/>
            <Route path="/proveedores-editar/:idproveedor" exact element={<ProveedoresEditar/>}/>
            <Route path="/categorias-admin" exact element={<CategoriasAdmin/>}/>
            <Route path="/categorias-crear" exact element={<CategoriasCrear/>}/>
            <Route path="/categorias-editar/:idcategoria" exact element={<CategoriasEditar/>}/>
            <Route path="/usuarios-admin" exact element={<UsuariosAdmin/>}/>
            <Route path="/usuarios-crear" exact element={<UsuariosCrear/>}/>
            <Route path="/usuarios-editar/:idusuario" exact element={<UsuariosEditar/>}/>

          </Routes>
        </Router>
      </Fragment>

 
    </div>
  );
}

export default App;

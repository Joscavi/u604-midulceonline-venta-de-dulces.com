import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const ProductosCrear = () => {

    const navigate = useNavigate();

    const [producto, setProducto] = useState({
            sku: '',
            nombre: '',
            precio: '',
            peso: '',
            categoria: '',
            proveedor: '',
            stock: ''
    });

    const {sku,
        nombre,
        precio,
        peso,
        categoria,
        proveedor,
        stock} = producto;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setProducto({
            ...producto,
            [e.target.name]: e.target.value
        })
    }

    const crearProducto = async () => {
        const data = {
            sku: producto.sku,
            nombre: producto.nombre,
            precio: producto.precio,
            peso: producto.peso,
            categoria: producto.categoria,
            proveedor: producto.proveedor,
            stock: producto.stock
        }

        const response = await APIInvoke.invokePOST(`/api/productos`, data);
        const idProducto = response._id;

        if (idProducto === '') {
            const msg = "El producto NO fue creado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate("/productos-admin");
            const msg = "El producto fue creado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });

            setProducto({
                sku:'',
                nombre: '',
                precio: '',
                peso: '',
                categoria: '',
                proveedor: '',
                stock: ''
             })
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearProducto();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Creación de Productos"}
                    breadCrumb1={"Listado de Producto"}
                    breadCrumb2={"Creación"}
                    ruta1={"/productos-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                {/* /SKU*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">SKU</label>
                                        <input type="text"
                                            className="form-control"
                                            id="sku"
                                            name="sku"
                                            placeholder="Ingrese el SKU del producto"
                                            value={sku}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /nombre*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre del producto"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /precio*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Precio</label>
                                        <input type="number"
                                            className="form-control"
                                            id="precio"
                                            name="precio"
                                            placeholder="Ingrese el precio"
                                            value={precio}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                 {/* /peso*/}
                                 <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Peso</label>
                                        <input type="text"
                                            className="form-control"
                                            id="peso"
                                            name="peso"
                                            placeholder="Ingrese el peso"
                                            value={peso}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Categoria*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Categoria</label>
                                        <input type="text"
                                            className="form-control"
                                            id="categoria"
                                            name="categoria"
                                            placeholder="Ingrese la categoria"
                                            value={categoria}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Proveedor*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Proveedor</label>
                                        <input type="text"
                                            className="form-control"
                                            id="proveedor"
                                            name="proveedor"
                                            placeholder="Proveedor"
                                            value={proveedor}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Stock*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Stock</label>
                                        <input type="number"
                                            className="form-control"
                                            id="stock"
                                            name="stock"
                                            placeholder="Ingrese el peso"
                                            value={stock}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Crear</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ProductosCrear;
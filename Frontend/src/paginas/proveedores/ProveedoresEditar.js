import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const ProveedoresEditar = () => {

    const navigate = useNavigate();

    const { idproveedor } = useParams();
    let arreglo = idproveedor.split('@');
    
    const nombreProveedor = arreglo[1];
    const direccionProveedor = arreglo[2];
    const telefonoProveedor = arreglo[3];
    const emailProveedor = arreglo[4];
    const documentoProveedor= arreglo[5];




    console.log(arreglo);

    const [proveedor, setProveedor] = useState({
        nombre: nombreProveedor,
        direccion: direccionProveedor,
        telefono: telefonoProveedor,
        email: emailProveedor,
        documento: documentoProveedor
    });

    const { nombre,
        direccion,
        telefono,
        email,
        documento,
    } = proveedor;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setProveedor({
            ...proveedor,
            [e.target.name]: e.target.value
        })
    }

    const editarProveedor = async () => {
        let arreglo = idproveedor.split('@');
        const idProveedor = arreglo[0];
        

        const data = {
            nombre: proveedor.nombre,
            direccion: proveedor.direccion,
            telefono: proveedor.telefono,
            email: proveedor.email,
            documento: proveedor.documento

        }

        const response = await APIInvoke.invokePUT(`/api/proveedores/${idProveedor}`, data);
        const idProveedorEditado = response._id

        if (idProveedorEditado !== idProveedor) {
            const msg = "El Proveedor no fue editado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/proveedores-admin");
            const msg = "El Proveedor fue editado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarProveedor();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de Proveedores"}
                    breadCrumb1={"Listado de Proveedores"}
                    breadCrumb2={"Edición"}
                    ruta1={"/proveedores-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                {/* /nombre*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre del Proveedor"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /direccion*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Dirección</label>
                                        <input type="text"
                                            className="form-control"
                                            id="direccion"
                                            name="direccion"
                                            placeholder="Ingrese la dirección"
                                            value={direccion}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Telefono*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Telefono</label>
                                        <input type="number"
                                            className="form-control"
                                            id="telefono"
                                            name="telefono"
                                            placeholder="Ingrese el Telefono"
                                            value={telefono}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Email*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Email</label>
                                        <input type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            placeholder="Ingrese el Email"
                                            value={email}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Documento*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Documento o Nit</label>
                                        <input type="number"
                                            className="form-control"
                                            id="documento"
                                            name="documento"
                                            placeholder="Ingrese el Documento o Nit"
                                            value={documento}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ProveedoresEditar;
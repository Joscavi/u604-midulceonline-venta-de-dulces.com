import React from "react";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import APIInvoke from "../../utils/APIInvoke";
import swal from 'sweetalert';

const CrearCuenta = () => {
 const [usuario, setUsuario] = useState({
   rol: '',
   email: '',
   nombre_completo: '',
   documento: '',
   direccion_de_facturacion: '',
   direccion_de_envio: '',
   ciudad: '',
   telefono: '',
   contraseña: '',
   confirmar: ''
 });
 const {rol,email,nombre_completo,documento,direccion_de_facturacion,
  direccion_de_envio,ciudad,telefono,contraseña,confirmar} = usuario;
 
 const onChange =(e) =>{
  setUsuario({
    ...usuario,[e.target.name ]: e.target.value
  });
 };
const crearCuenta =  async() =>
{
  if (contraseña !== confirmar) {
    const msg = "Las contraseñas son diferentes"
    swal({
      title: 'Error',
      text: msg,
      icon: 'error',
      button: {
        confirm: {
          text: 'OK',
          value: true,
          visible: true,
          className: 'btn btn-danger',
          closeModal: true
        }
      }

    });
  } 
  else if (contraseña.length < 6) {

    const msg = "La constraseña debe ser al menos de 6 caracteres"
    swal({
      title: 'Error',
      text: msg,
      icon: 'error',
      buttons: {
        confirm: {
          text: 'Ok',
          value: true,
          visible: true,
          className: 'btn btn-danger',
          closeModal: true
        }
      }
    });
  } else {

    const data = {
      nombre_completo: usuario.nombre_completo,
      rol: usuario.rol,
      email: usuario.email,
      contraseña: usuario.contraseña,
      documento: usuario.documento,
      direccion_de_facturacion:usuario.direccion_de_facturacion,
      direccion_de_envio:usuario.direccion_de_envio,
      ciudad:usuario.ciudad,
      telefono:usuario.telefono
    }
    const response = await APIInvoke.invokePOST("/api/usuarios", data);
    console.log(response)
    const mensaje = response.msg;
    if (mensaje === 'El usuario ya existe') {
      const msg = "El usuario ya existe.";
      swal({
        title: 'Error',
        text: msg,
        icon: 'error',
        buttons: {
          confirm: {
            text: 'Ok',
            value: true,
            visible: true,
            className: 'btn btn-danger',
            closeModal: true
          }
        }
      });
    } else {
      console.log("Usuario Creado Exitosamente")
      const msg = "Usuario Creado Exitosamente";
      swal({
        title: 'Felicidades',
        text: msg,
        icon: 'success',
        buttons: {
          confirm: {
            text: 'Ok',
            value: true,
            visible: true,
            className: 'btn btn-primary',
            closeModal: true
          }
        }
      });



    }
  }
}

 const onSubmit = (e) => {
   e.preventDefault();
   crearCuenta();
 };

  return (
    <div className="hold-transition login-page">
      <div className="login-box">
        <div className="login-logo">
          <Link to={"#"}>
            <b>Crear</b>Cuenta
          </Link>
        </div>
        <div className="card">
          <div className="card-body login-card-body">
            <p className="login-box-msg">Registro de información</p>
            <form onSubmit={onSubmit}>
           {/* /rol*/}
             <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Rol "
                  id="rol"
                  name="rol"
                  value ={rol}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-address-book" />
                  </div>
                </div>
              </div>
             {/* /nombre*/}
            <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Nombre"
                  id="nombre_completo"
                  name="nombre_completo"
                  value ={nombre_completo}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>
             {/* /email*/}
              <div className="input-group mb-3">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Email"
                  id="email"
                  name="email"
                  value ={email}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              {/* /documento*/}
              <div className="input-group mb-3">
                <input
                  type="number"
                  className="form-control"
                  placeholder="Documento "
                  id="documento"
                  name="documento"
                  value ={documento}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-id-card" />
                  </div>
                </div>
              </div>
              {/* /direccion_de_facturacion*/}
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Dirección de factura "
                  id="direccion_de_facturacion"
                  name="direccion_de_facturacion"
                  value ={direccion_de_facturacion}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-house-user" />
                  </div>
                </div>
              </div>
              {/* /direccion_de_envio*/}
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Dirección de envio "
                  id="direccion_de_envio"
                  name="direccion_de_envio"
                  value ={direccion_de_envio}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-route" />
                  </div>
                </div>
              </div>
              {/* /ciudad*/}
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Ciudad "
                  id="ciudad"
                  name="ciudad"
                  value ={ciudad}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-city" />
                  </div>
                </div>
              </div>
              {/* /telefono*/}
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Teléfono "
                  id="telefono"
                  name="telefono"
                  value ={telefono}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-phone" />
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="password"
                  className="form-control"
                  placeholder="Contraseña"
                  id="contraseña"
                  name="contraseña"
                  value ={contraseña}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>

              <div className="input-group mb-3">
                <input
                  type="password"
                  className="form-control"
                  placeholder="Confirmar contraseña"
                  id="conf_password"
                  name="confirmar"
                  value ={confirmar}
                  onChange ={onChange}
                  required
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>

              <div className="social-auth-links text-center mb-3">
                <button type="submit" className="btn btn-block btn-primary">
                  Aceptar
                </button>
                <Link to={"/"} className="btn btn-block btn-danger">
                  Volver
                </Link>
              </div>
            </form>
          </div>
          {/* /.login-card-body */}
        </div>
      </div>
    </div>
  );
}
 
export default CrearCuenta;
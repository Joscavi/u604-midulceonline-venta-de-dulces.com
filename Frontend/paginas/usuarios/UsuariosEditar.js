import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const UsuariosEditar = () => {

    const navigate = useNavigate();

    const { idusuario } = useParams();
    let arreglo = idusuario.split('@');
    
    const rolUsuario = arreglo[1];
    const emailUsuario = arreglo[2];
    const contraseñaUsuario = arreglo[3];
    const nombre_completoUsuario = arreglo[4];
    const documentoUsuario = arreglo[5];
    const direccion_de_facturacionUsuario= arreglo[6];
    const direccion_de_envioUsuario = arreglo[7];
    const ciudadUsuario = arreglo[8];
    const telefonoUsuario = arreglo[9];


    console.log(arreglo);

    const [usuario, setUsuario] = useState({
        rol: rolUsuario,
        email: emailUsuario,
        contraseña: contraseñaUsuario,
        nombre_completo: nombre_completoUsuario,
        documento: documentoUsuario,
        direccion_de_facturacion: direccion_de_facturacionUsuario,
        direccion_de_envio: direccion_de_envioUsuario,
        ciudad: ciudadUsuario,
        telefono: telefonoUsuario
    });

    const { rol,
        email,
        contraseña,
        nombre_completo,
        documento,
        direccion_de_facturacion,
        direccion_de_envio,
        ciudad,
        telefono
    } = usuario;

    useEffect(() => {
        document.getElementById("nombre_completo").focus();
    }, [])

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    const editarUsuario = async () => {
        let arreglo = idusuario.split('@');
        const idUsuario = arreglo[0];
        

        const data = {
            rol: usuario.rol,
            email: usuario.email ,
            contraseña: usuario.contraseña,
            nombre_completo: usuario.nombre_completo,
            documento: usuario.documento,
            direccion_de_facturacion: usuario.direccion_de_facturacion,
            direccion_de_envio: usuario.direccion_de_envio,
            ciudad: usuario.ciudad,
            telefono: usuario.telefono

        }

        const response = await APIInvoke.invokePUT(`/api/usuarios/${idUsuario}`, data);
        const idUsuarioEditado = response._id

        if (idUsuarioEditado !== idUsuario) {
            const msg = "El Usuario no fue editado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/usuarios-admin");
            const msg = "El Usuario fue editado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarUsuario();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de Usuarios"}
                    breadCrumb1={"Listado de Usuarios"}
                    breadCrumb2={"Edición"}
                    ruta1={"/usuarios-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                {/* /Rol*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Rol</label>
                                        <input type="text"
                                            className="form-control"
                                            id="rol"
                                            name="rol"
                                            placeholder="Ingrese el rol del Usuario"
                                            value={rol}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Email*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Email</label>
                                        <input type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            placeholder="Ingrese el email del Usuario"
                                            value={email}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Contraseña*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Contraseña</label>
                                        <input type="password"
                                            className="form-control"
                                            id="contraseña"
                                            name="contraseña"
                                            placeholder="Ingrese Contraseña"
                                            value={contraseña}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /nombre*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre Completo</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre_completo"
                                            name="nombre_completo"
                                            placeholder="Ingrese el nombre del Usuario"
                                            value={nombre_completo}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /documento*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Documento</label>
                                        <input type="number"
                                            className="form-control"
                                            id="documento"
                                            name="documento"
                                            placeholder="Ingrese el documento del Usuario"
                                            value={documento}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /direcciondefacturacion*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Dirección de Facturación</label>
                                        <input type="text"
                                            className="form-control"
                                            id="direccion_de_facturacion"
                                            name="direccion_de_facturacion"
                                            placeholder="Ingrese la dirección de facturación"
                                            value={direccion_de_facturacion}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /direcciondeenvio*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Dirección de Envío</label>
                                        <input type="text"
                                            className="form-control"
                                            id="direccion_de_envio"
                                            name="direccion_de_envio"
                                            placeholder="Ingrese la dirección de envío"
                                            value={direccion_de_envio}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /ciudad*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Ciudad</label>
                                        <input type="text"
                                            className="form-control"
                                            id="ciudad"
                                            name="ciudad"
                                            placeholder="Ingrese la Ciudad"
                                            value={ciudad}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {/* /Telefono*/}
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Telefono</label>
                                        <input type="number"
                                            className="form-control"
                                            id="telefono"
                                            name="telefono"
                                            placeholder="Ingrese el Telefono"
                                            value={telefono}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default UsuariosEditar;
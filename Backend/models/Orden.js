const mongoose = require('mongoose');

const ordenSchema = mongoose.Schema({
    usuario:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'usuario'
    },
    producto:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'producto'
    },
    cantidad:{
        type: Number,
        required: true
    },
    monto:{
        type: Number,
        required: true
    }
    
});

module.exports = mongoose.model('Ordenes', ordenSchema );
const mongoose = require('mongoose');

const factura_compraSchema = mongoose.Schema({
    fecha_compra:{
        type: String,
        required: true
    },
    valor_compra:{
        type: String,
        required: true
    },
    proveedor:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'proveedor'
    }
});

module.exports = mongoose.model('Facturas_compras', factura_compraSchema );
const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({
    sku:{
        type: Number,
        required: true
    },
    nombre:{
        type: String,
        required: true
    },
    precio:{
        type: Number,
        required: true
    },
    peso:{
        type: String,
        required: true
    },
    categoria:{
        type: String,
        required: true,
        ref:'categoria'
    },
    proveedor:{
        type: String,
        required: true,
        ref:'proveedor'
    },
    stock:{
        type: Number,
        required: true
    },
    
});

module.exports = mongoose.model('Productos', productoSchema );
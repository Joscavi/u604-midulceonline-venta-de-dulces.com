const mongoose = require('mongoose');
const bcryptjs = require("bcryptjs");
saltRounds = 10;

const usuarioSchema = mongoose.Schema({
    rol:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    contraseña:{
        type: String,
        required: true
    },
    nombre_completo:{
        type: String,
        required: true
    },
    documento:{
        type: Number,
        required: true
    },
    direccion_de_facturacion:{
        type: String,
        required: true
    },
    direccion_de_envio:{
        type: String,
        required: true
    },
    ciudad:{
        type: String,
        required: true
    },
    telefono:{
        type: Number,
        required: true
    }
});
usuarioSchema.pre("save", function (next) {
    bcryptjs.hash(this.contraseña, saltRounds, (err, hash) => {
      this.contraseña = hash;
      next();
    });
  });

module.exports = mongoose.model('Usuarios', usuarioSchema );
const mongoose = require('mongoose');

const factura_ventaSchema = mongoose.Schema({
    fecha_venta:{
        type: String,
        required: true
    },
    orden:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'orden'
    },
    valor_total:{
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Facturas_ventas', factura_ventaSchema );
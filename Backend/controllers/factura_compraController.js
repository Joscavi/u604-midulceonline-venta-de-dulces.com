const Factura_compra = require("../models/Factura_compra");

exports.crearFactura_compra = async (req,res) => {

    try{
        let factura_compra;
         // creamos nuestro usuario
         factura_compra = new Factura_compra(req.body);
         await factura_compra.save();
         res.send(factura_compra);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarFacturas_compras =async (req,res) =>{

try{
   const facturas_compras = await Factura_compra.find();
  res.json(facturas_compras)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerFactura_compra = async (req,res) =>{
 try{
let factura_compra = await Factura_compra.findById(req.params.id);
if (!factura_compra){
    res.status(404).json({msg: 'la factura no existe'})
}
res.json(factura_compra);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarFactura_compra = async (req,res) =>{
    try{
        let factura_compra = await Factura_compra.findById(req.params.id);
        if (!factura_compra){
            res.status(404).json({msg: 'la factura no existe'})
        }
        await Factura_compra.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'factura eliminado con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarFactura_compra = async (req,res) =>{
    try{
     const {fecha_compra, valor_compra, proveedor } = req.body;
     let factura_compra = await Factura_compra.findById(req.params.id); 
     if (!factura_compra){
        res.status(404).json({msg: 'la factura no existe'})
    }
    factura_compra.fecha_compra= fecha_compra;
    factura_compra.valor_compra= valor_compra;
    factura_compra.proveedor= proveedor;

    factura_compra = await Factura_compra.findOneAndUpdate({_id: req.params.id}, factura_compra, {new:true})
    res.json(factura_compra);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

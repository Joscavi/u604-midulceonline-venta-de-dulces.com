const Orden = require("../models/Orden");

exports.crearOrden = async (req,res) => {

    try{
        let orden;
         // creamos nuestro orden
         orden = new Orden(req.body);
         await orden.save();
         res.send(orden);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarOrdenes =async (req,res) =>{

try{
   const ordenes = await Orden.find();
  res.json(ordenes)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerOrden = async (req,res) =>{
 try{
let orden = await Orden.findById(req.params.id);
if (!orden){
    res.status(404).json({msg: 'la orden no existe'})
}
res.json(orden);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarOrden = async (req,res) =>{
    try{
        let orden = await Orden.findById(req.params.id);
        if (!orden){
            res.status(404).json({msg: 'la orden no existe'})
        }
        await Orden.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'orden eliminada con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarOrden = async (req,res) =>{
    try{
     const {usuario, producto, cantidad, monto } = req.body;
     let orden = await Orden.findById(req.params.id); 
     if (!orden){
        res.status(404).json({msg: 'la orden no existe'})
    }
    orden.usuario = usuario;
    orden.producto = producto;
    orden.cantidad = cantidad;
    orden.monto = monto;

    orden = await Orden.findOneAndUpdate({_id: req.params.id}, orden, {new:true})
    res.json(orden);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}
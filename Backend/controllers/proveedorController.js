const Proveedor = require("../models/Proveedor");

exports.crearProveedor = async (req,res) => {

    try{
        let proveedor;
         // creamos nuestro proveedor
         proveedor = new Proveedor(req.body);
         await proveedor.save();
         res.send(proveedor);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarProveedores =async (req,res) =>{

try{
   const proveedores = await Proveedor.find();
  res.json(proveedores)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerProveedor = async (req,res) =>{
 try{
let proveedor = await Proveedor.findById(req.params.id);
if (!proveedor){
    res.status(404).json({msg: 'el proveedor no existe'})
}
res.json(proveedor);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarProveedor = async (req,res) =>{
    try{
        let proveedor = await Proveedor.findById(req.params.id);
        if (!proveedor){
            res.status(404).json({msg: 'el proveedor no existe'})
        }
        await Proveedor.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'proveedor eliminado con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarProveedor = async (req,res) =>{
    try{
     const {nombre, documento, direccion, telefono, email } = req.body;
     let proveedor = await Proveedor.findById(req.params.id); 
     if (!proveedor){
        res.status(404).json({msg: 'el proveedor no existe'})
    }
    proveedor.nombre = nombre;
    proveedor.direccion = direccion;
    proveedor.telefono = telefono;
    proveedor.email = email;
    proveedor.documento = documento;

    proveedor = await Proveedor.findOneAndUpdate({_id: req.params.id}, proveedor, {new:true})
    res.json(proveedor);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

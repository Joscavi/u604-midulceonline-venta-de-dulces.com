const Factura_venta = require("../models/Factura_venta");

exports.crearFactura_venta = async (req,res) => {

    try{
        let factura_venta;
         // creamos nuestro factura_venta
         factura_venta = new Factura_venta(req.body);
         await factura_venta.save();
         res.send(factura_venta);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarFacturas_ventas =async (req,res) =>{

try{
   const facturas_ventas = await Factura_venta.find();
  res.json(facturas_ventas)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerFactura_venta = async (req,res) =>{
 try{
let factura_venta = await Factura_venta.findById(req.params.id);
if (!factura_venta){
    res.status(404).json({msg: 'la factura no existe'})
}
res.json(factura_venta);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarFactura_venta = async (req,res) =>{
    try{
        let factura_venta = await Factura_venta.findById(req.params.id);
        if (!factura_venta){
            res.status(404).json({msg: 'la factura no existe'})
        }
        await Factura_venta.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'factura eliminado con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarFactura_venta = async (req,res) =>{
    try{
     const {fecha_venta, orden, valor_total } = req.body;
     let factura_venta = await Factura_venta.findById(req.params.id); 
     if (!factura_venta){
        res.status(404).json({msg: 'la factura no existe'})
    }
    factura_venta.fecha_venta= fecha_venta;
    factura_venta.orden= orden;
    factura_venta.valor_total= valor_total;

    factura_venta = await Factura_venta.findOneAndUpdate({_id: req.params.id}, factura_venta, {new:true})
    res.json(factura_venta);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

const express = require('express');
const router = express.Router();
const factura_compraController = require('../controllers/factura_compraController');

// rutas CRUD

router.get('/', factura_compraController.mostrarFacturas_compras);
router.post('/', factura_compraController.crearFactura_compra);
router.get('/:id', factura_compraController.obtenerFactura_compra);
router.put('/:id', factura_compraController.actualizarFactura_compra);
router.delete('/:id', factura_compraController.eliminarFactura_compra);

module.exports = router;
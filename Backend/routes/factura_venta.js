const express = require('express');
const router = express.Router();
const factura_ventaController = require('../controllers/factura_ventaController');

// rutas CRUD

router.get('/', factura_ventaController.mostrarFacturas_ventas);
router.post('/', factura_ventaController.crearFactura_venta);
router.get('/:id', factura_ventaController.obtenerFactura_venta);
router.put('/:id', factura_ventaController.actualizarFactura_venta);
router.delete('/:id', factura_ventaController.eliminarFactura_venta);

module.exports = router;
